package nl.utwente.di.temperature;

public class TemperatureConverter {
    /**
     * Converts a temperature in Celsius to Fahrenheit.
     * @param temperature Temperature in Celsius.
     * @return Temperature in Fahrenheit.
     */
    public static double convertToFahrenheit(String temperature) {
        return Double.parseDouble(temperature) * 1.8 + 32;
    }
}
